import sys
from time import time

debug     = 1
unit_test = 0
prod_test = 1
scale_run = 0

def pprint_matrix(mat):
    """Print a matrix... prettily!"""
    col_size = 5
    for r in mat:
        lin = ''
        for el in r:
            lin += str(el)
            while len(lin)%col_size!=0:
                lin += ' '
        print(lin)

# Decorator class to debug with
class debug_deco(object):
    def __init__(self, f):
        self.f = f
    def __call__(self, *args):
        if debug:
            # prints debug, function name, and all arguments
            print(self.f.__name__ + " Args::")
            for i in range(len(args)):
                if i==0:
                    pprint_matrix(args[0])
                else: print(str(i)+str(args[i]))
        return self.f(*args)

@debug_deco 
def make_square(matrixa):
    res = matrixa[:]
    # Add rows...
    while len(res[0])>len(res):
        res.append([0]*len(res[0]))
    # ... or columns as necessary...
    while len(res[0])<len(res):
        res = [x + [0] for x in res]
    # ... and return the result.
    return res

@debug_deco
def controlStructure(matrixb):
    # Make incoming matrix square, if need be
    matrixc = make_square(matrixb[:])
    # Balance the incoming matrix and store the result in a buffer
    matrixc = balance(matrixc[:])
    # Attempt assignment
    result = complete(matrixc[:])
    # Run rebalance as long as assignment fails (even partially)
    while not result[0]:
        # Theta_Balance if completion failed
        matrixc = theta_balance(matrixc[:],result[1])
        # Attempt completion again
        result = complete(matrixc[:])
    # Return assignments
    return result[1]

@debug_deco
def theta_balance(matrix,selected):
    rowCount     = len(matrix)
    master       = set(range(rowCount))
    selectedRows = set([x[1] for x in selected])
    selectedCols = set([x[0] for x in selected])
    tickedRows   = master - selectedRows
    tickedCols   = set()
    changesStep  = len(tickedCols) + len(tickedRows) + 1    
    while changesStep != len(tickedCols) + len(tickedRows):
        changesStep = len(tickedCols) + len(tickedRows)
        # Add columns sharing 0 with a ticked row
        for rowInd in tickedRows:
            tickedCols |= set([x for x in master if matrix[rowInd][x]==0])
        tickedRows |= set([x[1] for x in selected if x[0] in tickedCols])
    
    theta = max( [max(y) for y in matrix] ) # establish upper bound on whole matrix
    for i in tickedRows:
        for j in master - tickedCols:
            theta = min([theta,matrix[i][j]])
    for i in range(rowCount):
        for j in range(rowCount):
            if i in tickedRows and j not in tickedCols:
                matrix[i][j] -= theta
            if i not in tickedRows and j in tickedCols:
                matrix[i][j] += theta
    return matrix

@debug_deco
def balance(matrixe):
    # Subtract row_min from each row
    matrixe=[[x-min(y) for x in y] for y in matrixe]
    # Subtract col_min from each col
    matrixe=[[x-min(y) for x in y] for y in rot90(matrixe)]
    return rot90(matrixe,3)

def rot90(matrixf,cnt=1):
    # No actual rotation
    if cnt%4==0:
        return m
    # Single 90 CW rotation
    elif cnt%4==1:
        return list([list(li) for li in zip(*matrixf[::-1])])
    else: # Recurse for more rotation than that
        return rot90(list([list(li) for li in zip(*matrixf[::-1])]),cnt-1)

@debug_deco
def row_col_subs(matrixg,r,c):
    #Can only subtract so many rows/cols
    if len(r)+len(c)>len(matrixg):
        raise ValueError
    elif len(r)+len(c)==len(matrixg):
        return []
    # Remove indicated rows/cols and return
    return [[x for i,x in enumerate(y) if i not in c] for j,y in enumerate(matrixg) if j not in r]

def comp_zeroes(mat, selRows, selCols):
    n = len(mat)
    z_r, z_c = [0]*n, [0]*n
    for i in range(n**2):
        if mat[i//n][i%n]==0:
            if (i//n) not in selRows and (i%n) not in selCols:
                z_r[i//n] += 1
                z_c[i%n]  += 1
    return z_r, z_c

@debug_deco
def complete(matrix):
    # Initialize variables
    buffer, assig = matrix[:], []
    s_r, s_c = set([]), set([])
    msa = set(range(len(buffer)))
    n = len(msa)
    # Attempt all assignments
    for i in range(len(matrix)):
        # Make lists of zero-counts by row and col
        z_r, z_c = comp_zeroes(buffer, s_r, s_c)
        # Make assignment on any row with 1 zero
        if 1 in z_r:
            y = z_r.index(1)
            x = buffer[y].index(0)
            while x in s_c:
                x = buffer[y].index(0,x+1)
        # Or the first column with one zero
        elif 1 in z_c:
            x = z_c.index(1)
            for i in msa - s_r:
                if buffer[i][x]==0:
                    y = i
        else:
            return 0, assig
        assig.append((x,y))
        s_r.add(y)
        s_c.add(x)
    return 1, assig

def unit_testing():
    global debug
    d, debug = debug, 0
    failed = 0
    tests = 0
    inp = [
        [[1,2,3],[4,5,6]],
        [[1,2],[3,4],[5,6]],
        [[1,2,3],[4,5,6],[7,8,9]]]
    out0 = [
        [[1,2,3],[4,5,6],[0,0,0]],
        [[1,2,0],[3,4,0],[5,6,0]],
        [[1,2,3],[4,5,6],[7,8,9]]]
    print("Testing fn: make_square...")
    ti = time()
    for i,inner in enumerate(inp):
        tests += 1
        try:
            assert make_square(inner)==out0[i]
        except AssertionError:
            failed += 1
    print(str(tests) + " runs took " + str(time()-ti) + " seconds.")
    print("Failed " + str(failed) + " tests out of " + str(tests) + ".\n")
    if failed!=0:
        sys.exit(1)
    
    failed = 0
    tests = 0
    inp = [
        [[4,0,3,10,2],[6,14,0,4,5],[0,0,2,0,0],[8,0,3,6,3],[4,0,8,11,1]],
        [[3,0,2,9,1],[6,15,0,4,5],[0,1,2,0,0],[7,0,2,5,2],[3,0,7,10,0]],
        [[2,0,1,8,0],[6,16,0,4,5],[0,2,2,0,0],[6,0,1,4,1],[3,1,7,10,0]],
        [[1,0,0,7,0],[6,17,0,4,6],[0,3,2,0,1],[5,0,0,3,1],[2,1,6,9,0]],
        [[0,0,0,6,0],[5,17,0,3,6],[0,4,3,0,2],[4,0,0,2,1],[1,1,6,8,0]]]
    res = [
        [(1,0),(2,1),(0,2)],
        [(1,0),(2,1),(0,2),(4,4)],
        [(2,1),(0,2),(4,4),(1,3)],
        [(1,3),(0,2),(2,1),(4,4)]]
    print("Testing fn: theta_balance...")
    ti = time()
    for i,el in enumerate(inp):
        if i:
            tests += 1
            try:
                t = theta_balance(inp[i-1],res[i-1])
                assert t==inp[i]
            except AssertionError:
                failed += 1
                print("res",t)
    print(str(tests) + " runs took " + str(time()-ti) + " seconds.")
    print("Failed " + str(failed) + " tests out of " + str(tests) + ".\n")
    if failed!=0:
        sys.exit(1)

    failed = 0
    tests = 1
    inp = [ [11, 7 , 10, 17, 10] ,
             [13, 21, 7 , 11, 13] ,
             [13, 13, 15, 13, 14] ,
             [18, 10, 13, 16, 14] ,
             [12, 8 , 16, 19, 10]  ]
    res = [[4,0,3,10,2],[6,14,0,4,5],[0,0,2,0,0],[8,0,3,6,3],[4,0,8,11,1]]
    print("Testing fn: balance...")
    ti = time()
    try:
        assert balance(inp)==res
    except AssertionError:
        failed+=1
    print(str(tests) + " runs took " + str(time()-ti) + " seconds.")
    print("Failed " + str(failed) + " tests out of " + str(tests) + ".\n")
    if failed!=0:
        sys.exit(1)

    failed = 0
    tests = 0
    inp = [
        [[4,0,3,10,2],[6,14,0,4,5],[0,0,2,0,0],[8,0,3,6,3],[4,0,8,11,1]],
        [[3,0,2,9,1],[6,15,0,4,5],[0,1,2,0,0],[7,0,2,5,2],[3,0,7,10,0]],        
        [[2,0,1,8,0],[6,16,0,4,5],[0,2,2,0,0],[6,0,1,4,1],[3,1,7,10,0]],        
        [[1,0,0,7,0],[6,17,0,4,6],[0,3,2,0,1],[5,0,0,3,1],[2,1,6,9,0]],        
        [[0,0,0,6,0],[5,17,0,3,6],[0,4,3,0,2],[4,0,0,2,1],[1,1,6,8,0]]]
    res = [
        ([1,0,0,1,1],[0,3,0,0,0]),
        ([1,1,3,1,2],[1,3,1,1,2]),
        ([2,1,3,1,1],[1,2,1,1,3]),
        ([3,1,2,2,1],[1,2,3,1,2]),
        ([4,1,2,2,1],[2,2,3,1,2])]
    print("Testing fn: comp_zeroes...")
    ti = time()
    for i in range(len(inp)):
        tests+=1
        try:
            assert comp_zeroes(inp[i],[] if i!=0 else [1,2],[] if i!=0 else [3,4])==res[i]
        except AssertionError:
            failed+=1
    print(str(tests) + " runs took " + str(time()-ti) + " seconds.")
    print("Failed " + str(failed) + " tests out of " + str(tests) + ".\n")
    if failed!=0:
        sys.exit(1)

    failed = 0
    tests = 0
    inp = [
        [[4,0,3,10,2],[6,14,0,4,5],[0,0,2,0,0],[8,0,3,6,3],[4,0,8,11,1]],
        [[3,0,2,9,1],[6,15,0,4,5],[0,1,2,0,0],[7,0,2,5,2],[3,0,7,10,0]],
        [[2,0,1,8,0],[6,16,0,4,5],[0,2,2,0,0],[6,0,1,4,1],[3,1,7,10,0]],
        [[1,0,0,7,0],[6,17,0,4,6],[0,3,2,0,1],[5,0,0,3,1],[2,1,6,9,0]],
        [[0,0,0,6,0],[5,17,0,3,6],[0,4,3,0,2],[4,0,0,2,1],[1,1,6,8,0]]
        ]
    res = [
        [0,[(0,2),(1,0),(2,1)]],
        [0,[(0,2),(1,0),(2,1),(4,4)]],
        [0,[(0,2),(1,3),(2,1),(4,0)]],
        [0,[(0,2),(1,3),(2,1),(4,0)]],
        [1,[(0,0),(1,3),(2,1),(3,2),(4,4)]]
        ]
    print("Testing fn: complete...")
    ti = time()
    for i in range(len(inp)):
        tests+=1
        try:
            t = complete(inp[i])
            assert t[0]==res[i][0]
            for el in res[i][1]:
                assert el in t[1]
        except AssertionError:
            failed += 1
            print("FAILURE DETAILS:")
            pprint_matrix(inp[i])
            print("\nres: " + str(t) + "\nexp: " + str(res[i]))
    print(str(tests) + " runs took " + str(time()-ti) + " seconds.")
    print("Failed " + str(failed) + " tests out of " + str(tests) + ".\n")
    if failed!=0:
        sys.exit(1)
    debug = d

def prod_testing():
    # Test matrix
    test = [[13.0, 14.0, 8.0, 15.0, 15.0, 13.0],
            [12.75, 15.0, 1.5, 18.0, 12.75, 12.0],
            [13.0, 14.0, 8.0, 15.0, 15.0, 13.0],
            [13.0, 9.0, 8.0, 15.0, 10.5, 13.0],
            [13.0, 14.0, 0.0, 10.5, 15.0, 13.0],
            [13.0, 14.0, 8.0, 15.0, 15.0, 13.0]]
    # Get assignments
    resu = controlStructure(test)
    if debug: print("Calculations complete\nResult:")
    # Print assignments
    for i in resu:
        print(i)
    # Print total cost
    z=0+sum([test[y][x] for x,y in resu])
    print(z)

def scale_running():
    with open(sys.argv[1]) as inf:
        for line in inf:
            print(line)
	
if __name__=="__main__":
    if unit_test: unit_testing()
    if prod_test: prod_testing()
    if scale_run: scale_running()


