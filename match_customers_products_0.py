# 48 - Travis Manning - 15/3/2015
import sys
from fractions import gcd

class customer: # Tracks customer names and potential scores
    def __init__(self,name):
        self.nam=name
        k=sum([1 for x in self.nam if x in 'auioe'])
        self.sco=[k,len(self.nam)-k] # possible scores, indexed by len(p.name)%2
        self.pre = self.sco.index(max(self.sco))
    def isrulethree(self,prodname): # potential for 1.5 multiplier
        if gcd(len(self.nam),len(prodname))>1:
            return 1
        return 0
    def score(self,prod): # returns score if product paired with customer
        h=self.isrulethree(prod.nam)
        return self.sco[prod.dir]*(1+h*(0.5))
    
class product: # tracks product names
    def __init__(self,name):
        self.nam = name
        self.dir = len(name)%2

def assign(m):
    sub_min_from_row(m)
    m=rot90(m)
    sub_min_from_row(m)
    m=rot90(m,3)
    return zero_counts(m)

def sub_min_from_row(m):
    for i in range(len(m)):
        n=min(m[i])
        for j in range(len(m)):
            m[i][j]-=n
    return m

def rot90(m,cnt=1): # NEEDS CLEAN INPUT - NO EXCEPTION HANDLING
    for i in range(cnt):
        m=list(zip(*m[::-1]))
    return m

def zero_counts(m):
    r, c, f = [], [], [] # row, col
    while not m:
        r=[sum([1 for x in l if x==0.]) for l in m]
        c=[sum([1 for x in l if x==0.]) for l in rot90(m)]
        while 1 in r:
            n=r.index(1) # row
            l=m[n].index(0.) # col
            r=removeat(r,n)
            c=removeat(c,l)
            for i in range(len(m)):
                m[i]=removeat(m[i],l)
            m=removeat(m,n)
            f.append((n,l))
        while 1 in c:
            n=c.index(1) # col
            for i in range(len(m)):
                if m[i][n]==0.:
                    l=i # row
            r=removeat(r,l)
            c=removeat(c,n)
            for i in range(len(m)):
                m[i]=removeat(m[i],n)
            m=removeat(m,l)
            f.append((l,n))
    return f

def removeat(l,n):
    return l[:n]+l[n+1:]

if __name__=="__main__":
    #with open(sys.argv[1]) as inf:
    #sample data - 6x6
    inf = ["Jeffery Lebowski,Walter Sobchak,Theodore Donald Kerabatsos,Peter Gibbons,Michael Bolton,Samir Nagheenanajar;Half & Half,Colt M1911A1,16lb bowling ball,Red Swingline Stapler,Printer paper,Vibe Magazine Subscriptions - 40 pack"]
    for line in inf:
        cstlst, prdlst = [], []
        if len(line)<3:
            continue # move on if line too short to be data
        # Set up customer and product lists
        prd, cst = line.split(';')
        prd, cst = prd.split(','), cst.split(',')
        for c in cst:
            cstlst.append(customer(c))
        for p in prd:
            prdlst.append(product(p))
        # Generate cost-benefit matrix
        mat = [[c.score(p) for c in cstlst] for p in prdlst]
        if len(cstlst)<len(prdlst):
            for i in mat:
                i+=[0]*(len(prdlst)-len(cstlst))
        elif len(prdlst)<len(cstlst):
            for i in range(len(cstlst)-len(prdlst)):
                mat+=[0]*(len(cstlst)-len(prdlst))
        # resolve cost-benefit matrix into sum of ss vals
        print(mat)
        res, s = assign(mat), 0
        for i in res:
            s += mat[i[1]][i[0]]
        else:
            print(None)
        #print result
        print(round(s,2))
