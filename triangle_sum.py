import sys
from random import random

def istriangle(r):
    if len(r)<2:
        return 0
    for i in range(len(r)):
        if len(r[i])-1!=i:
            break
    else:
        return 1
    return 0

def find_longest(t):
    if istriangle(t):
        return _find_longest(t)
    else:
        return 0

def iif(c,t,f): # Immediate if
    if c:
        return t
    else:
        return f

def _find_longest(t):
    if len(t)>2:
        cel = _find_longest(t[:-1]) # removes last ele
        res, m = [], len(cel)
        for i in range(len(t[m])):
            if i==0 or i==m:
                res.append(t[m][i]+t[m-1][iif(i==0,0,i-1)])
            else:
                res.append(max( (t[m][i]+cel[i-1],t[m][i]+cel[i]) ))
    else:
        return ((t[0][0]+t[1][0],t[0][0]+t[1][1]))
    return res     

if __name__=="__main__":
	tri = []
	with open(sys.argv[1]) as inf:
		for line in inf:
			temp = list(man(int,line.replace('\n','').split(' ')))
			tri = tri + temp
    print(max(find_longest(tri)))