import sys
def ispalindrome(n):
    if (str(n)==str(n)[::-1]):
        return 1
    return 0
def steppalin(n):
    return n+int(str(n)[::-1])
if __name__=="__main__":
    with open(sys.argv[1]) as inf:
        for line in inf:
            num, cnt = int(line.replace('\n','')), 0
            while not ispalindrome(num):
                num, cnt = steppalin(num), cnt + 1
            print(str(cnt)+" "+str(num))
    sys.exit(0)
            
            
            
