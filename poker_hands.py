import sys

class card(object):
    def __init__(self,_id):
        for i,x in enumerate('234567890JQKA'):
            for j,y in enumerate('HDSC'):
                if _id==x+y: self.val=i+j*13

def get_counts(h):
    res = dict()
    for c in h:
        if c in res.keys():
            res[c]+=1
        else:
            res[c]=1
    return res

class hand(object):
    def __init__(self,cards):
        if len(cards)!=5:
            raise ValueError
        self.hand = [card(x) for x in cards]
        self._analyze()
    def _analyze(self):
        s  = [c.val//13 for c in self.hand]
        ss = set(s)
        v  = [c.val%13 for x in self.hand]
        vc = get_counts(self.hand)
        if len(ss)==1: #flush, straight flush, royal flush
            if sv==set([12,11,10,9,8]): #royal flush
                self.value = 4100
                return
            elif set(vc.values())=set([1]): # five unique cards
                m=min(v)
                if set(v)=set([m,m+1,m+2,m+3,m+4]): # straight flush
                    self.value = max(v)+1+4019
                    return
            self.value = max(v)+1+3632
            return
        if set(vc.values())=set([1,4]): # four of a kind
            if vc(self.hand[0])==4:
                
            
if __name__=="__main__":
    with open(sys.argv[1]) as inf:
        for line in inf:
            cards = line.replace('\n','').split(' ')
            p1, p2 = hand(cards[:5]), hand(cards[5:])
            
