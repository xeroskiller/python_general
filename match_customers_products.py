# 48 - Travis Manning - 15/3/2015
import sys
from fractions import gcd

class customer: # Tracks customer names and potential scores
    def __init__(self,name):
        self.nam=name
        k=sum([1 for x in self.nam if x in 'auioe'])
        self.sco=[k,len(self.nam)-k] # possible scores, indexed by len(p.name)%2
        self.pre = self.sco.index(max(self.sco))
    def isrulethree(self,prodname): # potential for 1.5 multiplier
        if gcd(len(self.nam),len(prodname))>1:
            return 1
        return 0
    def score(self,prod): # returns score if product paired with customer
        h=self.isrulethree(prod.nam)
        return self.sco[prod.dir]*(1+h*(0.5))
    
class product: # tracks product names
    def __init__(self,name):
        self.nam = name
        self.dir = len(name)%2
        
def assign(m):
    # Row ops: subtract m[i] from max(m)
    # this turns this into a minimization problem.
    # in turn enabling the use of the hungarian algorithm.
    for i in range(len(m)):
        a = max(m[i])
        for j in range(len(m)):
            m[i][j] = a - m[i][j]
    # Get any completed objects
    z=[]
    z = [checkcomplete(m)]
    print("M:")
    for i in m:
        print(i)
    if len(z[0])==len(m):
        return z
    n=rot90(m) # rotate to work columns as rows
    for i in range(len(n)):
        a = min(n[i])
        for j in range(len(n[i])):
            n[i][j] = n[i][j] - a
    m=rot90(n[:],3) # clone to produce new object, unrotate
    z.append(checkcomplete(m))
    print(z[1])
    if len(z[1])==len(m):
        return z

def rot90(mat, cnt=1):
    if len(mat)==len(mat[0]):
        for i in range(cnt):
            mat = list(zip(*mat[::-1]))
        for i in range(len(mat)):
            mat[i]=list(mat[i])
        return mat
    return -1

# returns optimal choices (if any)
def checkcomplete(m):
    k = []
    for i in range(len(m)):
        try:
            a=m[i].index(0.)
        except ValueError:
            print("cont")
            continue
        for j in k:
            if j[0]==a or j[1]==i:
                break
        else:
            k.append((a,i))
    return k

if __name__=="__main__":
    #with open(sys.argv[1]) as inf:
    #sample data - 6x6
    inf = ["Jeffery Lebowski,Walter Sobchak,Theodore Donald Kerabatsos,Peter Gibbons,Michael Bolton,Samir Nagheenanajar;Half & Half,Colt M1911A1,16lb bowling ball,Red Swingline Stapler,Printer paper,Vibe Magazine Subscriptions - 40 pack"]
    for line in inf:
        cstlst, prdlst = [], []
        if len(line)<3:
            continue # move on if line too short to be data
        # Set up customer and product lists
        prd, cst = line.split(';')
        prd, cst = prd.split(','), cst.split(',')
        for c in cst:
            cstlst.append(customer(c))
        for p in prd:
            prdlst.append(product(p))
        # Generate cost-benefit matrix
        mat = [[c.score(p) for c in cstlst] for p in prdlst]
        if len(cstlst)<len(prdlst):
            for i in mat:
                i+=[0]*(len(prdlst)-len(cstlst))
        elif len(prdlst)<len(cstlst):
            for i in range(len(cstlst)-len(prdlst)):
                mat+=[0]*(len(cstlst)-len(prdlst))
        # resolve cost-benefit matrix into sum of ss vals
        print(mat)
        res, s = assign(mat), 0
        for i in res:
            s += mat[i[1]][i[0]]
        else:
            print(None)
        #print result
        print(round(s,2))
