import sys

def lines(path):
    with open(path) as inf:
        for line in inf:
            yield line.strip()

def getMajorElement(l):
    d, n = dict(), len(l)
    for el in l:
        if el in d:
            d[el] += 1
        else:
            d[el] = 1
    for k in d:
        if d[k] >= n // 2:
            return k
    return 'null'

def main():
    for line in lines(sys.argv[1]):
        c = getMajorElement(line.split(','))
        if c == 'null':
            print('None')
        else:
            print(c)

if __name__ == "__main__":
    main()
    sys.exit(0)
