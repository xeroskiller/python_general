import sys
from math import sqrt

def countsquares(n):
    cnt=0
    if n==0:
        return ''
    for i in range(int(sqrt(n))+1):
        sqt=sqrt(n-(i**2))
        if sqt//1==sqt:
            cnt+=1
    return cnt//2

if __name__=="__main__":
    num=0
    with open(sys.argv[1]) as inf:
        for line in inf:
            if num==0:
                num=int(line.replace('\n',''))
            else:
                print(countsquares(int(line.replace('\n',''))))
