import sys

class pyramid:
    def __init__(self):
        self.l = []
    def addline(self,k):
        if len(self.l)+1!=len(k):
            raise ValueError
        self.l.append(k)
    def longest(self):
        return max(findlongest(self.l))
    
def findlongest(li):
    if len(li)>2:
        k=findlongest(li[:-1])
    else:
        return [li[0][0]+li[1][0],li[0][0]+li[1][1]]
    res, m = [], len(li[len(k)])-1
    for i in range(len(li)):
        if i==0:
            res.append(li[m][0]+k[0])
        elif i==(len(li)-1):
            res.append(li[m][m]+k[m-1])
        else:
            res.append(max((li[m][i]+k[i-1],li[m][i]+k[i])))
    return (res)

if __name__=="__main__":
    res = pyramid()
    with open(sys.argv[1],'r') as inf:
        for line in inf:
            b=line.replace('\n','').split(' ')
            h=list(map(int,[x for x in b if len(x)>0]))
            res.addline(h)
    print(res.longest())
