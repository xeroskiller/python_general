# ~1300 rids
# ~3700 HDDID's

# HCCID -- 'subscriberId', 'id'
# RID   -- 'idNumber'

# Only uses id's of type MDWMD4

import xml.etree.ElementTree as ET
import re

dataset = []
infile = 'c:\\users\\tmanning\\documents\\hip_ceinv_20150313.xml'
outfile = 'c:\\data\\filtered_xml_results.txt'

class member(object):
    def __init__(self,rid,hccid,fname,lname):
        self.rid = rid
        self.hccid = hccid
        self.lname = lname
        self.fname = fname

if __name__=="__main__":
    tree = ET.parse(infile)
    root = tree.getroot()
    #Get all ID's
    for c1 in root: #instance whatever
        for c2 in c1: #mem/corr
            if c2.tag=="member":
                temp = ['','','',''] #fn,ln,rid,hccid
                rbuf = ''
                for c3 in c2.iter():
                    if c3.tag=='idNumber':
                        rbuf = c3.text
                    elif c3.tag=='idTypeCode' and c3.text=='MDWMD4':
                        temp[2] = rbuf
                    elif c3.tag=='firstName':
                        temp[0]=c3.text
                    elif c3.tag=='lastName':
                        temp[1]=c3.text
                    elif c3.tag=='id':
                        temp[3]=c3.text
                #    h='id' in c3.tag or 'Id' in c3.tag
                #    if h and c3.text not in temp:
                #        temp.append(c3.text)
                dataset.append(temp)
    #filtered = []
    #temp=[]
    #Filter out non-id's and dupes
    #for mem in dataset:
    #    temp=[]
    #    for ob in mem:
    #        if re.match('10[0-9]{8}99',ob):
    #            id_=ob
    #        if re.match('[0-9]{10}-[0-9]{2}',ob):
    #            temp.append(ob)
    #        if ob=="MDWMD4" and id_!='':
    #            temp.append(id_)
    #    filtered.append(temp)
    
    with open(outfile,'w') as ouf:
        for mem in dataset:
            temp=''
            for ob in mem:
                if temp=='':
                    temp=ob
                else:
                    temp+=','+ob
            ouf.write(temp+'\n')
